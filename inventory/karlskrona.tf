# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 Xenial Xerus"

# masters
flavor_k8s_master = "667935bd-6b96-4547-85fc-639820f7087b" # 1C-4GB-20GB

# networking
external_net = "fba95253-5543-4078-b793-e2de58c31378"
floatingip_pool = "ext-net"
