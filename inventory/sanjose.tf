# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"

# masters
flavor_k8s_master = "ac2c4d17-8d6f-4e3c-a9eb-57c155f0a949"

# networking
external_net = "0048fce6-c715-4106-a810-473620326cb0"
floatingip_pool = "public"
