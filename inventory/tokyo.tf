# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 Xenial Xerus"

# masters
flavor_k8s_master = "cbe02391-7136-4b5d-ae92-f1eb0f75d8ca" # 2C-8GB-20GB

# networking
external_net = "ab3a75a8-6031-46a6-807d-2cbb0b55b832"
floatingip_pool = "ext-net"
