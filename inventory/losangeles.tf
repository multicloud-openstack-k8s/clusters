# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 Xenial Xerus"

# masters
flavor_k8s_master = "65c853cc-2270-448b-ae2d-9051bec7611d" # 1C-8GB-50GB

# networking
external_net = "aff174ee-008e-4190-8bfc-f6e4a37cc2e7"
floatingip_pool = "ext-net"
