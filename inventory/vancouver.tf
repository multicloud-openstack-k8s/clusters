# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu16.04-x86_64-20180223"

# masters
flavor_k8s_master = "4c3e637c-61f4-46cd-9fbd-d856c7814fb2"

# networking
external_net = "bbe81757-095c-42d4-bcbb-6077e29836fb"
floatingip_pool = "provider"
