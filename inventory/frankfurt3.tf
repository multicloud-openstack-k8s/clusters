# image to use for bastion, masters, standalone etcd instances, and nodes
image = "GNU/Linux Ubuntu Server 16.04 Xenial Xerus x64"

# masters
flavor_k8s_master = "340"

# networking
external_net = "50ea4b59-42e9-4427-9f00-16362fd0cfd9"
floatingip_pool = "PublicNetwork"
