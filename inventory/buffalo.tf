# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 Xenial Xerus"

# masters
flavor_k8s_master = "1b825858-832b-4c70-be0f-53c5b1a31b16" # 1C-8GB-50GB

# networking
external_net = "0232c17f-2096-49bc-b205-d3dcd9a30ebf"
floatingip_pool = "ext-net"
