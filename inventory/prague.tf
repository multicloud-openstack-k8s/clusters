# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"

# masters
flavor_k8s_master = "19a8bc69-5cb3-4b5e-b7ed-5123e879ca1b" # sa.large

# networking
external_net = "47ab2bf0-65ec-4a3d-bce6-fe6b6e5dc346"
floatingip_pool = "public"
