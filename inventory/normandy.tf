# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"
ssh_user = "cloud"

# masters
flavor_k8s_master = "42" # n2.cw.standard-1

# networking
external_net = "6ea98324-0f14-49f6-97c0-885d1b8dc517"
floatingip_pool = "public"
