# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu-16.04"

az_list = ["iad-2"]

# masters
flavor_k8s_master = "300" # gp1.lightspeed

# networking
external_net = "e098d02f-bb35-4085-ae12-664aad3d9c52"
floatingip_pool = "public"
