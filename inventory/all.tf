# SSH key to use for access to nodes
public_key_path = "~/.ssh/id_rsa.pub"

# image to use for bastion, masters, standalone etcd instances, and nodes
# user on the node (ex. core on Container Linux, ubuntu on Ubuntu, etc.)
ssh_user = "ubuntu"

# 0|1 bastion nodes
number_of_bastions = 0

# standalone etcds
number_of_etcd = 0

# masters
number_of_k8s_masters = 1
number_of_k8s_masters_no_etcd = 0
number_of_k8s_masters_no_floating_ip = 0
number_of_k8s_masters_no_floating_ip_no_etcd = 0
supplementary_master_groups = "kube-node,kube-ingress"

# nodes
number_of_k8s_nodes = 0
number_of_k8s_nodes_no_floating_ip = 0
flavor_k8s_node = ""

# networking
network_name = "kube-network"
subnet_cidr = "10.11.12.0/24"
dns_nameservers = ["8.8.8.8", "8.8.4.4"]
