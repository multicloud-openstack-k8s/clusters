# image to use for bastion, masters, standalone etcd instances, and nodes
image = "GNU/Linux Ubuntu Server 16.04 Xenial Xerus x64"

# masters
flavor_k8s_master = "340"

# networking
external_net = "1d09bbe1-7f46-4856-b0b6-4f983f196342"
floatingip_pool = "PublicNetwork"
