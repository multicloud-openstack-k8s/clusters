# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"
ssh_user = "cloud"

# masters
flavor_k8s_master = "42" # n2.cw.standard-1

# networking
external_net = "b5dd7532-1533-4b9c-8bf9-e66631a9be1d"
floatingip_pool = "public"
