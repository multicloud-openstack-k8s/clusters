#!/bin/bash
set -e
# Checking inputs

if [ -z "$1" ]
  then
    echo "Error: No argument supplied"
    echo ""
    echo "Usage: $0 <cluster name>"
    exit 1
fi

cluster="$1"
var_file="inventory/$cluster.tf"

if [ ! -f "$var_file" ]
then
  echo "Error: Terraform var file does not exist: '$var_file' "
  exit 1
fi

zone="root314.com"
echo "Terraforming $cluster ..."
cp backend.tf kubespray/contrib/terraform/openstack/
export OS_CLOUD="$cluster"
terraform init kubespray/contrib/terraform/openstack/
terraform workspace new "$cluster" 2> /dev/null || echo "Terraform workspace already present"
terraform workspace select "$cluster"
terraform apply -var "cluster_name=$cluster" -var-file=inventory/all.tf -var-file="$var_file" kubespray/contrib/terraform/openstack

echo "Terraforming finished"

echo "Waiting for SSH to be up in $cluster ..."
ansible-playbook -i inventory.sh wait-for.yaml
echo "SSH is up"

echo "Kubespraying $cluster ..."
# Run ansible with dynamic inventory
ansible-playbook --become -i inventory.sh kubespray/cluster.yml --extra-vars apiserver_loadbalancer_domain_name="$cluster.k8s.$zone"
echo "Kubespraying finished"

echo "Run post-install playbook"
ansible-playbook -i inventory.sh post-install.yaml --extra-vars "cluster=$cluster"
echo "Post-install playbook done"

echo "Adding $cluster in DNS ..."

ip=$(./inventory.sh --list|jq --raw-output 'first( ._meta.hostvars[] | .access_ip_v4)')
./dns.py --zone "$zone" --sub app.k8s add "$ip"
./dns.py --zone "$zone" --sub "$cluster.k8s" add "$ip"
./dns.py --zone "$zone" --sub "*.$cluster.k8s" add "$ip"
echo "DNS udpated"

echo "Adding $cluster to GitLab..."
# Read the ssh_user but default to ubuntu
ssh_user=ubuntu
source <(grep ssh_user "inventory/$cluster.tf" | sed 's/ //g')
export KUBE_CA_CERT=$(ssh "$ssh_user@$ip" sudo cat /etc/kubernetes/ssl/ca.pem)
export KUBE_TOKEN=$(ssh "$ssh_user@$ip" kubectl get secret gitlab -o jsonpath='{.data.token}' | python -m base64 -d)
./gitlab/update_cluster.py --cluster "$cluster" --kube-apiserver "https://$cluster.k8s.root314.com:6443"  --project "$APP_PROJECT"
echo "GitLab updated"
