Kubernetes setup on OpenStack Public Clouds.

## Usage
Setup a cluster with `./up.sh <clustername>` then destroy a cluster with `./down.sh <clustername>`

## Configuration
Put the OpenStack API config in `clouds.yaml` and expose the API password with the `OS_PASSWORD` environment variable.
Infrastructure configuration is in `inventory/<clustername>.tf`, make sure to customize `floatingip_pool`, `external_net`, `flavor_k8s_master` and `image`

## GitLab integration
Copy the Kubernetes CA:
```
sudo cat /etc/kubernetes/ssl/ca.pem
```

And the GitLab token:
```
kubectl get secret gitlab -o jsonpath='{.data.token}' | base64 --decode
```
