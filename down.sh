#!/bin/sh
set -e
# Checking inputs

if [ -z "$1" ]
  then
    echo "Error: No argument supplied"
    echo ""
    echo "Usage: $0 <cluster name>"
    exit 1
fi

cluster="$1"
var_file="inventory/$cluster.tf"

if [ ! -f "$var_file" ]
then
  echo "Error: Terraform var file does not exist: '$var_file' "
  exit 1
fi



echo "Terraforming $cluster ..."
cp backend.tf kubespray/contrib/terraform/openstack/
export OS_CLOUD="$cluster"
terraform init kubespray/contrib/terraform/openstack/
terraform workspace new "$cluster" 2> /dev/null || echo "Terraform workspace already present"
terraform workspace select "$cluster"

echo "Removing $cluster in DNS ..."
zone="root314.com"
ip=$(./inventory.sh --list|jq --raw-output 'first( ._meta.hostvars[] | .access_ip_v4)')
./dns.py --zone "$zone" --sub app.k8s delete "$ip"
./dns.py --zone "$zone" --sub "$cluster.k8s" delete "$ip"
./dns.py --zone "$zone" --sub "*.$cluster.k8s" delete "$ip"
echo "DNS udpated"
echo "Waiting 60s for DNS propagation..."
sleep 60

terraform destroy -var "cluster_name=$cluster" -var-file=inventory/all.tf -var-file="$var_file" kubespray/contrib/terraform/openstack

echo "Terraforming finished"
